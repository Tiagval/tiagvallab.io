#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Tiago Vale'
SITENAME = 'Tiago Vale Personal Site'
SITEURL = ''
THEME='./Flex'
DESCRIPTION = 'Tiago Vale'
SITESUBTITLE = 'Tiago Vale'
SITELOGO = '/images/profile.png'

PYGMENTS_STYLE = 'monokai'
RFG_FAVICONS = True
MAIN_MENU = False

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Lisbon'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Social widget
SOCIAL = (('linkedin', 'https://www.linkedin.com/in/tiago-vale/'),
         ('twitter', 'https://twitter.com/_tiagovale'),
         ('envelope','mailto:tiagval@gmail.com'),
         ('gitlab','https://gitlab.com/Tiagval'),
         )

#MENUITEMS = (('Home','/'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
