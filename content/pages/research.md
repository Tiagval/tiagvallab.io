Title: Research

[CaloQVAE : Simulating high-energy particle-calorimeter interactions using hybrid quantum-classical generative models](https://arxiv.org/abs/2312.03179)

[Search for pair-production of vector-like quarks in pp collision events at √s=13 TeV with at least one leptonically decaying Z boson and a third-generation quark with the ATLAS detector](https://www.sciencedirect.com/science/article/pii/S0370269323003532?via%3Dihub)

[Use of a Generalized Energy Mover's Distance in the Search for Rare Phenomena at Colliders](https://link.springer.com/article/10.1140%2Fepjc%2Fs10052-021-08891-6)

[Transferability of Deep Learning Models in Searches for New Physics at Colliders](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.101.035042)


[Combination of the Searches for Pair-Produced Vectorlike Partners of the Third-Generation Quarks at √s=13 TeV with the ATLAS Detector](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.121.211801)


[Search for pair and single production of vectorlike quarks in final states with at least one Z boson decaying into a pair of electrons or muons in pp collision data collected with the ATLAS detector at √s=13 TeV](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.98.112010)

