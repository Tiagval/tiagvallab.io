Title: Welcome
status: hidden
save_as: index.html
Slug: about

Welcome to my personal page!

I am a physicist with a focus on data analysis and machine learning. I worked with the ATLAS experiment at CERN using machine learning to explore the large amounts of data produced by the LHC. During this time I acquired competences in data analysis, using C++ to process data in the search for new physics phenomena, and using machine learning techniques to go beyond the traditional techniques in order to exploit all the available data. 

Now I apply my skills in industry as a data scientist.

I love coding and data, go, music and books.
